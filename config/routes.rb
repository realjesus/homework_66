Rails.application.routes.draw do
  post 'like/:post_id' => 'likes#create', as: 'like'
  post 'dislike/:post_id' => 'likes#destroy', as: 'dislike'
  root 'posts#index'
  devise_for :users
  resources :user, only: :show
  resources :posts, only: [:create, :new]
  resources :comments , only: [:new, :create]
end

