module ApplicationHelper

	def is_already_liked?(post)
		if Like.find_by( user_id: current_user.id, post_id: post.id ) == nil 
			false
		else 
			true
		end
	end
end
