class Post < ApplicationRecord

	validates :title, presence: true,
                    length: { minimum: 1, maximum: 250 }

  validate :correct_image_type

  belongs_to :user
  has_many_attached :images
  has_many :comments
  has_many :likes 


  private 

  def correct_image_type

  	if images.attached? && !images.all? {|image| image.content_type.in?( %w( image/jpeg image/png )) }
      errors.add(:images , 'Must be a JPEG or a PNG')
  	elsif images.attached? == false 
  		errors.add(:images , 'required')
  	end

  end

end
